#include <iostream>
#include <cmath>

struct Point3D
{
    double x;
    double y;
    double z;
};

class Vector
{
public:
    Vector() {}
    Vector(const double val) : point{val, val, val} {}
    Vector(const double _x, const double _y, const double _z) : point{ _x, _y, _z } {}
    Vector(const Point3D& _point) { point = _point; }

    Point3D GetPoint() { return point; }

    void SetPoint(const double _x, const double _y, const double _z)
    {
        point.x = _x;
        point.y = _y;
        point.z = _z;
    }

    void Print()
    { 
        std::cout << "(" << point.x << ", " << point.y << ", " << point.z << ")";
    }

    bool IsNull()
    {
        return point.x == 0.0 && point.y == 0.0 && point.z == 0.0;
    }

    double GetLength();

private:
    Point3D point{0, 0, 0};
};

double Vector::GetLength()
{
    if (IsNull())
        return 0.0;
    else
    {
        double sqrSum = pow(point.x, 2) + pow(point.y, 2) + pow(point.z, 2); 
        return sqrt(sqrSum);
    }
}

int main()
{
    Vector a;
    std::cout << "Vector a ";
    a.Print();
    std::cout << " Lenght:  " << a.GetLength() << '\n';

    Vector b(5.0);
    std::cout << "Vector b ";
    b.Print();
    std::cout << " Lenght:  " << b.GetLength() << '\n';

    Vector c(-2.7, 9.0, 4.5);
    std::cout << "Vector c ";
    c.Print();
    std::cout << " Lenght:  " << c.GetLength() << '\n';

    Vector d(Point3D({ 5.8, -0.7, 1.9 }));
    std::cout << "Vector d ";
    d.Print();
    std::cout << " Lenght:  " << d.GetLength() << '\n';

    return 0;
}

